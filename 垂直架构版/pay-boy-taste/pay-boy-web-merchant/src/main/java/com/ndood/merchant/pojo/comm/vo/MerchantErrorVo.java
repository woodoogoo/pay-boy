package com.ndood.merchant.pojo.comm.vo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class MerchantErrorVo implements Serializable {
	private static final long serialVersionUID = 4948544702730668719L;

	private String code;
	private String msg;

	public MerchantErrorVo() {
		super();
	}

	public MerchantErrorVo(String code, String msg) {
		super();
		this.code = code;
		this.msg = msg;
	}

}
