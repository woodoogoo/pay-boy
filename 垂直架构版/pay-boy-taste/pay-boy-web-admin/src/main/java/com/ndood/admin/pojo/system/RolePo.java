package com.ndood.admin.pojo.system;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * 角色bean
 * @author ndood
 */
@Entity
@Table(name="t_role")
@Cacheable(true)
@Getter @Setter
public class RolePo implements Serializable{
	private static final long serialVersionUID = 7148771564181945122L;

	/**
	 * ID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	/**
	 * 名称
	 */
	@Column(length = 32, nullable = false, unique = true)
	private String name;
	
	/**
	 * 描述
	 */
	@Column(name="`desc`",length = 32)
	private String desc;

	/**
	 * 删除标记
	 */
	@Column(nullable = false)
	private Integer status;
	
	/**
	 * 排序
	 */
	private Integer sort;
	
	/**
	 * 创建时间
	 */
	@Column(name="create_time", nullable = false, columnDefinition="datetime", updatable=false)
	private Date createTime;
	
	/**
	 * 修改时间
	 */
	@Column(name="update_time", nullable = false, columnDefinition="datetime")
	private Date updateTime;
	
	/**
     * 被分配给的用户
     */
    @ManyToMany
    @JoinTable(name = "t_user_role", joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"), 
	inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
    private List<UserPo> users;
	
	/**
	 * 角色拥有的资源
	 */
	@ManyToMany
	@JoinTable(name = "t_role_permission", joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"), 
	inverseJoinColumns = @JoinColumn(name = "permission_id", referencedColumnName = "id"))
	private List<PermissionPo> permissions;

}
