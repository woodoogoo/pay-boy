package com.ndood.admin.pojo.system;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
@Entity
@Table(name="t_job")
@Cacheable(true)
@Getter @Setter
public class JobPo implements Serializable{
	
	private static final long serialVersionUID = -6445598210559852351L;
	/**
	 * id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	/**
	 * cron表达式
	 */
	@Column(name="cron_expression")
	private String cronExpression;
	/**
	 * 任务调用的方法名
	 */
	@Column(name="method_name")
	private String methodName;
	/**
	 * 任务是否有状态
	 */
	@Column(name="is_concurrent")
	private String isConcurrent;
	/**
	 * 任务描述
	 */
	@Column(name="description")
	private String description;
	/**
	 * 更新者
	 */
	@Column(name="update_by")
	private String updateBy;
	/**
	 * 任务执行时调用哪个类的方法 包名+类名
	 */
	@Column(name="bean_class")
	private String beanClass;
	/**
	 * 任务状态
	 */
	@Column(name="job_status")
	private String jobStatus;
	/**
	 * 任务分组
	 */
	@Column(name="job_group")
	private String jobGroup;
	/**
	 * 创建者
	 */
	@Column(name="create_by")
	private String createBy;
	/**
	 * Spring bean
	 */
	@Column(name="spring_bean")
	private String springBean;
	/**
	 * 任务名
	 */
	@Column(name="job_name")
	private String jobName;
	/**
	 * 创建时间
	 */
	@Column(name="create_time")
	private Date createTime;
	/**
	 * 更新时间
	 */
	@Column(name="update_time")
	private Date updateTime;
	
}