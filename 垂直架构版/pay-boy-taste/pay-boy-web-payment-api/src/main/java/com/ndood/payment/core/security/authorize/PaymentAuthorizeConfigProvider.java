package com.ndood.payment.core.security.authorize;

import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.stereotype.Component;

import com.ndood.core.authorize.AuthorizeConfigProvider;

/**
 * 实现AuthorizeConfigProvider
 * 确保基本配置在最开始生效
 */
@Component
@Order(Integer.MIN_VALUE)
public class PaymentAuthorizeConfigProvider implements AuthorizeConfigProvider{
	
	@Override
	public void config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config) {
		
		config.anyRequest().authenticated();
	}
}
